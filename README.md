# Sentien Press Kit

**About Us**

We believe there is a more natural and personal way to interact with technology and with each other. Interaction that is efficient and less distracting, without sacrificing either our human or digital well-being. Our mission is to create solutions that bring us closer to establishing a seamless link between people and technology.

We have a long way to go before we reach the ultimate goal of interacting with technology by simply thinking. Today, we are taking a step closer to that reality with Sentien Audio providing instantaneous access to multiple audio functions at a single touch or word.

Sentien Audio sits comfortably over your ears and conducts sound directly to your inner ear. It leaves your outer ear open, allowing you to hear your surroundings. Sentien Audio is always available, so the functionality you’re looking for to fit your daily needs is instantly accessible.

**Get in touch**

[Email](mailto:hello-at-sentienhq.cm)

[Blog](https://blog.sentienhq.com/)

**Social media**

[Facebook](https://www.facebook.com/SentienHQ)

[Reddit](https://www.reddit.com/user/SentienHQ/)

[Instagram](https://www.instagram.com/sentienhq/)

[Twitter](https://twitter.com/sentienhq)
